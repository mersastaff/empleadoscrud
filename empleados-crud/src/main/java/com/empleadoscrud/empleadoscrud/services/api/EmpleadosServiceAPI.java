package com.empleadoscrud.empleadoscrud.services.api;

import com.empleadoscrud.empleadoscrud.commons.GenericServiceAPI;
import com.empleadoscrud.empleadoscrud.model.Empleados;

public interface EmpleadosServiceAPI extends GenericServiceAPI <Empleados,Integer>  {
}
