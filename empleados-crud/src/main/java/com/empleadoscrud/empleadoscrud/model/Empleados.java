package com.empleadoscrud.empleadoscrud.model;



import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Empleados {

	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	@Column
	private String rut;
	
	@Column
	private String nombre;
	
	
	@Column
	private String perfil;
	
	@Column
	private String perfildos;
	
	
	public Empleados() {
		
	}

	

	
	
	
	public Empleados(Integer id, String rut, String nombre, String perfil, String perfildos) {
		super();
		this.id = id;
		this.rut = rut;
		this.nombre = nombre;
		this.perfil = perfil;
		this.perfildos = perfildos;
	}






	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	


	public String getPerfil() {
		return perfil;
	}



	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}






	public String getPerfildos() {
		return perfildos;
	}






	public void setPerfildos(String perfildos) {
		this.perfildos = perfildos;
	}






	
	
	
	

	
}
