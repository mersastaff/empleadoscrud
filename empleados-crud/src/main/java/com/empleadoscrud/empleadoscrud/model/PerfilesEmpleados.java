package com.empleadoscrud.empleadoscrud.model;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class PerfilesEmpleados {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Empleados empleado;
	
	@ManyToOne(fetch=FetchType.LAZY)
	private Perfiles perfil;
	
	
	
	public PerfilesEmpleados() {
		
	}
	

	public PerfilesEmpleados(Integer id, Empleados empleado, Perfiles perfil) {
		// super();
		this.id = id;
		this.empleado = empleado;
		this.perfil = perfil;
	}
	
	
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Empleados getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleados empleado) {
		this.empleado = empleado;
	}

	public Perfiles getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfiles perfil) {
		this.perfil = perfil;
	}
	
	
	
	
	
	
	
	
	

}
