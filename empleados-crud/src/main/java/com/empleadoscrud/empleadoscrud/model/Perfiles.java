package com.empleadoscrud.empleadoscrud.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Perfiles {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	
	@Column
	private String nombre;
	
	@Column
	private String descripcion;
	
	
	@OneToMany(fetch=FetchType.LAZY, mappedBy="perfil")
	private Set<PerfilesEmpleados> empleados = new HashSet<PerfilesEmpleados>(0);
	
	
	public Perfiles() {
		
	}
	

	public Perfiles(Integer id, String nombre, String descripcion) {
		// super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Set<PerfilesEmpleados> getEmpleados() {
		return empleados;
	}


	public void setEmpleados(Set<PerfilesEmpleados> empleados) {
		this.empleados = empleados;
	}
	
	
	
	
	
	
	
	

}
