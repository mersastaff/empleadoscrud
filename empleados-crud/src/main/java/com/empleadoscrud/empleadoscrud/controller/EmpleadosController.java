package com.empleadoscrud.empleadoscrud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.empleadoscrud.empleadoscrud.model.Empleados;
import com.empleadoscrud.empleadoscrud.services.api.EmpleadosServiceAPI;

@Controller
public class EmpleadosController {

	
	
	@Autowired
	private EmpleadosServiceAPI empleadosServiceAPI;

	
	
	
	@RequestMapping("/")
	public String index(Model model) {
		
		
		model.addAttribute("lista", empleadosServiceAPI.getAll());
		return "index";
	}

	
	@GetMapping("/save/{id}")
	public String showSave(@PathVariable("id") Integer id, Model model) {
	if(id!=null && id!=0) {
		model.addAttribute("empleado", empleadosServiceAPI.get(id));
	}else {
		model.addAttribute("empleado", new Empleados());
	}
	return "save";
	}
	
	@PostMapping("/save")
	public String save(Empleados empleado, Model model) {
		empleadosServiceAPI.save(empleado);
		return "redirect:/";
		
		
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable Integer id, Model model) {
		empleadosServiceAPI.delete(id);
		
		return "redirect:/";
	}
	
	
}
