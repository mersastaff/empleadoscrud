package com.empleadoscrud.empleadoscrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleadoscrud.empleadoscrud.model.Empleados;
import com.empleadoscrud.empleadoscrud.services.api.EmpleadosServiceAPI;

@RestController
@RequestMapping("/api/v1/")
public class EmpleadoRestController {

	@Autowired
	private EmpleadosServiceAPI empleadosServiceAPI;
	
	
	@GetMapping(value="/all")
	public List<Empleados> getAll(){
		return empleadosServiceAPI.getAll();
	}
	
	@GetMapping(value="/find/{id}")
	public Empleados find(@PathVariable Integer id) {
		return empleadosServiceAPI.get(id);
	}
	
	
	
	@PostMapping(value="/save")
	public ResponseEntity<Empleados> save(@RequestBody Empleados empleado){
		Empleados obj=empleadosServiceAPI.save(empleado);
		return new ResponseEntity<Empleados>(obj, HttpStatus.OK);
	}
	
	@GetMapping(value="/delete/{id}")
	public ResponseEntity<Empleados> delete(@PathVariable Integer id){
		Empleados empleado=empleadosServiceAPI.get(id);
		if(empleado!=null) {
			empleadosServiceAPI.delete(id);
		}else {
			return new ResponseEntity<Empleados>(empleado, HttpStatus.INTERNAL_SERVER_ERROR);
			}
			return new ResponseEntity<Empleados>(empleado, HttpStatus.OK);
	}
	
	
}
