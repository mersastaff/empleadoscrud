package com.empleadoscrud.empleadoscrud.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.empleadoscrud.empleadoscrud.commons.GenericServiceImpl;
import com.empleadoscrud.empleadoscrud.dao.api.EmpleadosDaoAPI;
import com.empleadoscrud.empleadoscrud.model.Empleados;
import com.empleadoscrud.empleadoscrud.services.api.EmpleadosServiceAPI;

@Service
public class EmpleadosServiceImpl extends GenericServiceImpl <Empleados, Integer> implements EmpleadosServiceAPI{
	@Autowired
	private EmpleadosDaoAPI empleadosDaoAPI;
	@Override
	public CrudRepository <Empleados, Integer> getDao(){	
			return empleadosDaoAPI;
			
		}
	
}
