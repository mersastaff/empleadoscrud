package com.empleadoscrud.empleadoscrud.dao.api;

import org.springframework.data.repository.CrudRepository;

import com.empleadoscrud.empleadoscrud.model.Empleados;

public interface EmpleadosDaoAPI extends CrudRepository<Empleados, Integer> {

}
